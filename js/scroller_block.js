/**
 * @file
 * Javascript for the Scroller Block module.
 */

(function($) {
  function scrollerBlock() {
    var $loop = Drupal.settings.scroller_block.loop;

    if ($loop != 'infinite') {
      $loop = Number($loop);
    }

    $('.scrollerblock_scroller').SetScroller({ velocity:    Number(Drupal.settings.scroller_block.velocity),
                                               direction:   Drupal.settings.scroller_block.direction,
                                               startfrom:   Drupal.settings.scroller_block.startfrom,
                                               loop:        $loop,
                                               movetype:    Drupal.settings.scroller_block.movetype,
                                               onmouseover: Drupal.settings.scroller_block.mouseover,
                                               onmouseout:  Drupal.settings.scroller_block.mouseout,
                                               onstartup:   Drupal.settings.scroller_block.startup,
                                               cursor:      Drupal.settings.scroller_block.cursor
    });

    var $bouncedirection = Drupal.settings.scroller_block.direction;

    $('#scrollerblock_bounceeffect').bind('bouncer', function(){
      if ($bouncedirection == 'vertical') {
        $(this).animate({left:Number(Drupal.settings.scroller_block.bounceheight1)}, Number(Drupal.settings.scroller_block.bouncespeed1), 'linear').animate({left:Number(Drupal.settings.scroller_block.bounceheight2)}, Number(Drupal.settings.scroller_block.bouncespeed2), 'linear', function(){$('#scrollerblock_bounceeffect').trigger('bouncer');});
      }
      else if ($bouncedirection == 'horizontal') {
        $(this).animate({top:Number(Drupal.settings.scroller_block.bounceheight1)}, Number(Drupal.settings.scroller_block.bouncespeed1), 'linear').animate({top:Number(Drupal.settings.scroller_block.bounceheight2)}, Number(Drupal.settings.scroller_block.bouncespeed2), 'linear', function(){$('#scrollerblock_bounceeffect').trigger('bouncer');});
      }
    }).trigger('bouncer');
  }

  Drupal.behaviors.scroller_block = {
     attach: function(context, settings) {
       scrollerBlock();
     }
  };
}(jQuery));
