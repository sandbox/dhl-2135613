<?php

/**
 * @file
 * Scroller Block page control.
 */

/**
 * A function call by hook_help().
 *
 * This is use to lower the amount of code in scroller_block.module.
 */
function scroller_block_help_delegate($path, $arg) {
  switch ($path) {
    case 'admin/help#scroller_block':
      $output = '<h3>' . t('About') . '</h3>';
      $output = '<p>' . t("In computer displays, filmmaking, television production, and other kinetic displays, scrolling is sliding text, images or video across a monitor or display, vertically or horizontally. \"Scrolling\", as such, does not change the layout of the text or pictures, but moves (pans or tilts) the user's view across what is apparently a larger image that is not wholly seen. A common television and movie special effect is to scroll credits, while leaving the background stationary.") . '</p>';
      $output = '<p>' . t('Reference to: !wikilink | !maxvergelli.', array(
        '!wikilink' => l(t('Wikipedia'), 'http://en.wikipedia.org/wiki/Scrolling'),
        '!maxvergelli' => l(t('Max Vergelli sourcecode repository'), 'http://www.maxvergelli.com/jquery-scroller'),
          )) . '</p>';
      $output = '<h3>' . t('Uses') . '</h3>';
      $output = '<dl>';
      $output = '<dt>' . t('Configuration') . '</dt>';
      $output = '<dd><ol>';
      $output = '<li>' . t("The <strong>Message</strong> textarea is the most versatile one. By using the WYSIWYG HTML editor (not include in this module), you can insert almost anything in the block region. The content inside it also treat as the highest priority, which means the block will only display the <strong>Message</strong>'s content, even the <strong>URL Image</strong> or <strong>Local Image</strong> form setting had been set.") . '</li>';
      $output = '<li>' . t('The <strong>URL Image</strong> textbox use to display a single image file inside the block region -- By typing the absolute URL path to the image file. Make sure the textarea of <strong>Message</strong> is blank if using this.') . '</li>';
      $output = '<li>' . t("The <strong>Local Image</strong> works like <strong>URL Image</strong>. The different is it use the image file in your local system. Make sure the <strong>Message</strong>'s textarea and <strong>URL Image</strong>'s textbox are blank if using this.") . '</li>';
      $output = '<li>' . t('By enabling the <strong>Bounce Effect</strong>, the image inside the block region will bounce up and down if you choose <strong>Horizontal</strong> direction. Bounce left and right if you choose <strong>Vertical</strong> direction.') . '</li>';
      $output = '<li>' . t('<strong>Bounce Height</strong> determine the bounce distance. For example, if you choose <strong>Horizontal</strong> direction, the bounce height is the distance from the toppest area of the block region.') . '</li>';
      $output = '<li>' . t('<strong>Bounce Height 2</strong> is use to change the bounce start position. For example, if you choose <strong>Horizontal</strong> direction, the number of <strong>Bounce Height 2</strong> is the distance away from the toppest area of the block region. The number of <strong>Bounce Height 2</strong> should be smaller than the number of <strong>Bounce Height</strong>.') . '</li>';
      $output = '<img src="' . file_create_url(drupal_get_path('module', 'scroller_block') . '/images/scroller_block_bounce_height_1.jpg') . '" />';
      $output = '<img src="' . file_create_url(drupal_get_path('module', 'scroller_block') . '/images/scroller_block_bounce_height_2.jpg') . '" />';
      $output = '<li>' . t('The bouncing speed can adjust too. For example, if you choose <strong>Horizontal</strong> direction, if <strong>Bounce Speed</strong> is the speed that go upward, then <strong>Bounce Speed 2</strong> is the speed that go downward.') . '</li>';
      $output = '<li>' . t("Note that <strong>Bounce Effect</strong> only apply to <strong>URL Image</strong> and <strong>Local Image</strong>. It does not apply to the image inside <strong>Message</strong>'s textarea.") . '</li>';
      $output = '<li>' . t('The higher the number of <strong>Scroll Speed</strong>, the faster the scrolling speed of the content.') . '</li>';
      $output = '<li>' . t("Don't forget to change <strong>Scroll Startfrom</strong> after change <strong>Scroll Direction</strong>. For example, <strong>Top</strong> and <strong>Bottom</strong> only work at <strong>Vertical</strong> direction.") . '</li>';
      $output = '<li>' . t('Type any positive number for <strong>Loop</strong>. The input number stand for the number of times will loop. Type "infinite" for infinite loop.') . '</li>';
      $output = '<li>' . t('The <strong>Linear</strong> movetype is the normal scrolling style. <strong>PingPong</strong> movetype will make the content bounce back at both end of the scroller block area. This is different to <strong>Bounce Effect</strong> which bounce at the sides of the block area.') . '</li>';
      $output = '<li>' . t('Can control the play or pause of the scrolling content. Note that the <strong>Bounce Effect</strong> cannot pause.') . '</li>';
      $output = '<li>' . t('Can change the cursor style that hover on the scroller block region. Refer to !w3school for details.', array(
        '!w3school' => l(t('W3School'), 'http://www.w3schools.com/cssref/pr_class_cursor.asp'),
          )) . '</li>';
      $output = '</ol></dd>';
      $output = '</dl>';
      return $output;
  }
}
