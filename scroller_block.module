<?php

/**
 * @file
 * A lightweight version of Drupal slideshow that place in block region.
 */

define('SCROLLER_BLOCK_PATH_IMAGES', 'scroller_block');

/**
 * Implements hook_help().
 */
function scroller_block_help($path, $arg) {
  module_load_include('inc', 'scroller_block', 'includes/scroller_block.page');
  return module_invoke('scroller_block', 'help_delegate', $path, $arg);
}

/**
 * Implements hook_libraries_info().
 */
function scroller_block_libraries_info() {
  $libraries['scroller-block'] = array(
    'name' => 'jQuery Scroller',
    'vendor url' => 'http://www.maxvergelli.com/jquery-scroller/',
    'download url' => 'http://www.maxvergelli.com/jquery-scroller/download/',
    'version arguments' => array(
      'file' => 'jquery-scroller.js',
      'pattern' => '%jQuery\s+Scroller\s+v\.?(\d.+){1}%',
      'lines' => 4,
      'cols' => 40,
    ),
    'files' => array(
      'js' => array(
        'jquery-scroller.js',
      ),
    ),
  );
  return $libraries;
}

/**
 * Implements hook_block_info().
 */
function scroller_block_block_info() {
  $blocks = array();
  $blocks['scroller-block'] = array(
    'info' => t('Scroller Block'),
    'cache' => DRUPAL_CACHE_GLOBAL,
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function scroller_block_block_view($delta = '') {
  $block = array();
  if ($delta == 'scroller-block') {
    $block['content'] = array(
      '#theme' => 'scroller_block',
      '#attached' => array(
        'libraries_load' => array(
          array('scroller-block'),
        ),
      ),
    );
  }
  return $block;
}

/**
 * Implements hook_block_configure().
 */
function scroller_block_block_configure($delta = '') {
  $form = array();
  if ($delta == 'scroller-block') {

    $scroll_bounceeffect = array(
      'yes' => t('Enable'),
      'no' => t('Disable'),
    );
    $scroll_directions = array(
      'horizontal' => t('Horizontal'),
      'vertical' => t('Vertical'),
    );
    $scroll_startfrom = array(
      'right' => t('Right'),
      'left' => t('Left'),
      'top' => t('Top'),
      'bottom' => t('Bottom'),
    );
    $scroll_movetype = array(
      'linear' => t('Linear'),
      'pingpong' => t('PingPong'),
    );
    $scroll_mouseover = array(
      'play' => t('Play'),
      'pause' => t('Pause'),
    );
    $scroll_mouseout = array(
      'play' => t('Play'),
      'pause' => t('Pause'),
    );
    $scroll_startup = array(
      'play' => t('Play'),
      'pause' => t('Pause'),
    );
    $form['scroller_block_message'] = array(
      '#type' => 'text_format',
      '#title' => t('Message'),
      '#description' => t('Enter the message here.'),
      '#default_value' => variable_get('scroller_block_message', ''),
    );

    $form['scroller_block_urlimage'] = array(
      '#type' => 'textfield',
      '#title' => t('URL Image'),
      '#description' => t('Type the url path to the image.'),
      '#default_value' => variable_get('scroller_block_urlimage', ''),
    );

    $form['scroller_block_localimage'] = array(
      '#type' => 'managed_file',
      '#title' => t('Local Image'),
      '#description' => t('Only *.gif, *.png, *.jpg and *.jpeg allowed.'),
      '#default_value' => variable_get('scroller_block_localimage', 0),
      '#upload_location' => 'public://' . SCROLLER_BLOCK_PATH_IMAGES,
      '#upload_validators' => array(
        'file_validate_extensions' => array('gif png jpg jpeg'),
      ),
    );

    $form['scroller_block_bounceeffect'] = array(
      '#type' => 'radios',
      '#title' => t('Image Bounce Effect'),
      '#description' => t('For image only'),
      '#options' => $scroll_bounceeffect,
      '#default_value' => variable_get('scroller_block_bounceeffect', 'no'),
    );

    $form['scroller_block_bounceheight1'] = array(
      '#type' => 'textfield',
      '#title' => t('Bounce Height'),
      '#description' => t('For bounce effect only'),
      '#default_value' => variable_get('scroller_block_bounceheight1', 40),
    );

    $form['scroller_block_bounceheight2'] = array(
      '#type' => 'textfield',
      '#title' => t('Bounce Height 2'),
      '#description' => t('For bounce effect only'),
      '#default_value' => variable_get('scroller_block_bounceheight2', 0),
    );

    $form['scroller_block_bouncespeed1'] = array(
      '#type' => 'textfield',
      '#title' => t('Bounce Speed'),
      '#description' => t('For bounce effect only'),
      '#default_value' => variable_get('scroller_block_bouncespeed1', 500),
    );

    $form['scroller_block_bouncespeed2'] = array(
      '#type' => 'textfield',
      '#title' => t('Bounce Speed 2'),
      '#description' => t('For bounce effect only'),
      '#default_value' => variable_get('scroller_block_bouncespeed2', 500),
    );

    $form['scroller_block_velocity'] = array(
      '#type' => 'textfield',
      '#title' => t('Scroll Speed'),
      '#description' => t('From 1 to 99.'),
      '#default_value' => variable_get('scroller_block_velocity', 50),
    );

    $form['scroller_block_direction'] = array(
      '#type' => 'radios',
      '#title' => t('Scroll Direction'),
      '#options' => $scroll_directions,
      '#default_value' => variable_get('scroller_block_direction', 'horizontal'),
    );

    $form['scroller_block_startfrom'] = array(
      '#type' => 'select',
      '#title' => t('Scroll Startfrom'),
      '#options' => $scroll_startfrom,
      '#default_value' => variable_get('scroller_block_startfrom', 'right'),
    );

    $form['scroller_block_loop'] = array(
      '#type' => 'textfield',
      '#title' => t('Loop'),
      '#description' => t('From 1 to n+, or infinite.'),
      '#default_value' => variable_get('scroller_block_loop', 'infinite'),
    );

    $form['scroller_block_movetype'] = array(
      '#type' => 'radios',
      '#title' => t('Scroll Movetype'),
      '#options' => $scroll_movetype,
      '#default_value' => variable_get('scroller_block_movetype', 'linear'),
    );

    $form['scroller_block_mouseover'] = array(
      '#type' => 'radios',
      '#title' => t('On Mouse Over'),
      '#options' => $scroll_mouseover,
      '#default_value' => variable_get('scroller_block_mouseover', 'pause'),
    );

    $form['scroller_block_mouseout'] = array(
      '#type' => 'radios',
      '#title' => t('On Mouse Out'),
      '#options' => $scroll_mouseout,
      '#default_value' => variable_get('scroller_block_mouseout', 'play'),
    );

    $form['scroller_block_startup'] = array(
      '#type' => 'radios',
      '#title' => t('On Start Up'),
      '#options' => $scroll_startup,
      '#default_value' => variable_get('scroller_block_startup', 'play'),
    );

    $form['scroller_block_cursor'] = array(
      '#type' => 'textfield',
      '#title' => t('Cursor Type'),
      '#description' => t('Any CSS cursor style.'),
      '#default_value' => variable_get('scroller_block_cursor', 'pointer'),
    );
  }
  return $form;
}

/**
 * Implements hook_block_save().
 */
function scroller_block_block_save($delta = '', $edit = array()) {
  if ($delta == 'scroller-block') {
    $imageblock = block_load('scroller_block', $delta);
    $prev_fid = variable_get('scroller_block_localimage', 0);
    if ($prev_fid != $edit['scroller_block_localimage']) {
      if ($prev_fid > 0) {
        $imagefile = file_load($prev_fid);
        if ($imagefile != NULL) {
          file_usage_delete($imagefile, 'scroller_block', 'block', $imageblock->bid);
          file_delete($imagefile);
        }
      }
    }
    variable_set('scroller_block_localimage', $edit['scroller_block_localimage']);
    if ($edit['scroller_block_localimage'] > 0 && $edit['scroller_block_localimage'] != $prev_fid) {
      $imagefile = file_load($edit['scroller_block_localimage']);
      $imagefile->statue = FILE_STATUS_PERMANENT;
      file_save($imagefile);
      file_usage_add($imagefile, 'scroller_block', 'block', $imageblock->bid);
    }
    $save = array(
      'scroller_block_message' => $edit['scroller_block_message']['value'],
      'scroller_block_urlimage' => $edit['scroller_block_urlimage'],
      'scroller_block_localimage' => $edit['scroller_block_localimage'],
      'scroller_block_bounceeffect' => $edit['scroller_block_bounceeffect'],
      'scroller_block_bounceheight1' => $edit['scroller_block_bounceheight1'],
      'scroller_block_bounceheight2' => $edit['scroller_block_bounceheight2'],
      'scroller_block_bouncespeed1' => $edit['scroller_block_bouncespeed1'],
      'scroller_block_bouncespeed2' => $edit['scroller_block_bouncespeed2'],
      'scroller_block_velocity' => $edit['scroller_block_velocity'],
      'scroller_block_direction' => $edit['scroller_block_direction'],
      'scroller_block_startfrom' => $edit['scroller_block_startfrom'],
      'scroller_block_loop' => $edit['scroller_block_loop'],
      'scroller_block_movetype' => $edit['scroller_block_movetype'],
      'scroller_block_mouseover' => $edit['scroller_block_mouseover'],
      'scroller_block_mouseout' => $edit['scroller_block_mouseout'],
      'scroller_block_startup' => $edit['scroller_block_startup'],
      'scroller_block_cursor' => $edit['scroller_block_cursor'],
    );
    array_map('variable_set', array_keys($save), array_values($save));
    drupal_set_message(t('Your new scroller settings have been saved.'));
  }
}

/**
 * Implements hook_theme().
 */
function scroller_block_theme() {
  $items = array();
  $items['scroller_block'] = array();
  return $items;
}

/**
 * Return HTML for scroller_block.
 *
 * @return string
 *   TRUE if any one of $message, $urlimage or $localimage is not empty string.
 *
 * @ingroup themeable
 */
function theme_scroller_block() {
  $path = drupal_get_path('module', 'scroller_block');
  drupal_add_css($path . '/css/scroller_block.css');
  drupal_add_js($path . '/js/scroller_block.js');

  $message = variable_get('scroller_block_message', '');
  $urlimage = variable_get('scroller_block_urlimage', '');
  $localimagefile = file_load(variable_get('scroller_block_localimage', ''));
  $localimagepath = '';
  if (isset($localimagefile->uri)) {
    $localimagepath = $localimagefile->uri;
  }
  $localimageoutput = theme('image', array(
    'path' => ($localimagepath),
  ));
  $bounceeffect = variable_get('scroller_block_bounceeffect', 'no');

  $settings = array(
    'bounceheight1' => variable_get('scroller_block_bounceheight1', 40),
    'bounceheight2' => variable_get('scroller_block_bounceheight2', 0),
    'bouncespeed1' => variable_get('scroller_block_bouncespeed1', 500),
    'bouncespeed2' => variable_get('scroller_block_bouncespeed2', 500),
    'velocity' => variable_get('scroller_block_velocity', 50),
    'direction' => variable_get('scroller_block_direction', 'horizontal'),
    'startfrom' => variable_get('scroller_block_startfrom', 'right'),
    'loop' => variable_get('scroller_block_loop', 'infinite'),
    'movetype' => variable_get('scroller_block_movetype', 'linear'),
    'mouseover' => variable_get('scroller_block_mouseover', 'pause'),
    'mouseout' => variable_get('scroller_block_mouseout', 'play'),
    'startup' => variable_get('scroller_block_startup', 'play'),
    'cursor' => variable_get('scroller_block_cursor', 'pointer'),
  );
  drupal_add_js(array('scroller_block' => $settings), 'setting');

  switch (TRUE) {
    case (strlen(trim($message)) > 0):
      $output = '';
      $output .= '<div class="scrollerblock_scroller">';
      $output .= '<div class="scrollerblock_scrollingtext">';
      $output .= '<span>' . $message . '</span>';
      $output .= '</div>';
      $output .= '</div>';
      break;

    case (strlen(trim($urlimage)) > 0):
      $output = '';
      $output .= '<div class="scrollerblock_scroller" id="scrollerblock_imagecontainer">';
      $output .= '<div class="scrollerblock_scrollingtext">';
      if ($bounceeffect == 'no') {
        $output .= '<img src="' . $urlimage . '" />';
      }
      else {
        $output .= '<img src="' . $urlimage . '" id="scrollerblock_bounceeffect" />';
      }
      $output .= '</div>';
      $output .= '</div>';
      break;

    case ($localimageoutput != ''):
      if ($bounceeffect == 'no') {
        $bounceprefix = '<div class="scrollerblock_scroller" id="scrollerblock_imagecontainer"> <div class="scrollerblock_scrollingtext">';
        $bouncesuffix = '</div> </div>';
      }
      else {
        $bounceprefix = '<div class="scrollerblock_scroller" id="scrollerblock_imagecontainer"> <div class="scrollerblock_scrollingtext"> <div id="scrollerblock_bounceeffect">';
        $bouncesuffix = '</div> </div> </div>';
      }
      $output = array(
        'image' => array(
          '#prefix' => $bounceprefix,
          '#type' => 'markup',
          '#markup' => $localimageoutput,
          '#suffix' => $bouncesuffix,
        ),
      );
      break;
  }
  return $output;
}
