-- SUMMARY --

Scroller Block is a lightweight version of Drupal slideshow that place in 
a Drupal block region. It is designed for the use of news ticker, website 
highlight or just decoration.

This module is an improved version of another module - Marquee Block
  https://drupal.org/project/marquee_block

The reason of not apply a patch for Marquee Block, is because this module 
no longer using the jquery marquee plugin and the html marquee tag.
There's nothing related to marquee anymore.

Comparsion between Scroller Block and Marquee Block:
-------------------------------------------------------------------------
|                           Scroller Block          Marquee Block       |
-------------------------------------------------------------------------
| Message editor         || WYSIWYG allowed || Text area (HTML allowed) |
-------------------------------------------------------------------------
| Use of image           ||   Local or url  ||  Need to type <img> tag  |
-------------------------------------------------------------------------
| Scroll speed           ||    Adjustable   ||        Adjustable        |
-------------------------------------------------------------------------
| Scroll direction       ||   4 directions  ||       2 directions       |
-------------------------------------------------------------------------
| Scroll behavior        ||     2 types     ||         3 types          |
-------------------------------------------------------------------------
| Numbers of loop        ||    Adjustable   ||      Not adjustable      |
-------------------------------------------------------------------------
| Control play/pause     ||       Yes       ||            No            |
-------------------------------------------------------------------------
| Bounce effect          ||       Yes       ||            No            |
-------------------------------------------------------------------------
| Cursor type when hover ||    Adjustable   ||      Not adjustable      |
-------------------------------------------------------------------------
 
-- REQUIREMENTS --

* This module requires you to download the jQuery Scroller libraries,
  available at:

    http://www.maxvergelli.com/jquery-scroller/

* This libraries was NOT programmed by this module's maintainer. It was 
  programmed by the programmer of Max Verglli.

-- INSTALLATION --

* See https://drupal.org/documentation/install/modules-themes/modules-7 
  for further information.

* Download "jquery-scroller-v1.src.js", rename it to 
  "jquery-scroller.js". Then put it into the following directories:
  
  (/sites/all/libraries/scroller-block/)

-- CONFIGURATION --

* After installation, go to Home >> Administration >> Structure >> 
  Blocks. Find "Scroller Block" and click "configure".

* Can choose Message, Url Image or Local Image. The priority is from top
  to bottom. For example, if want to use Local Image, make sure the text
  box of Message and Url Image are blank.

* For Url Image, type the absolute url path to the image file.

* Image Bounce Effect does NOT apply to Message.

* Type the number for Bounce Height. The larger the number the higher the
  bounce.

* Type the number for Bounce Height 2. This use to tune the Bounce
  Height.

* Type the number for Bounce Speed. The larger the number the slower the 
  speed. The 2 Bounce Speed parameter control the speed of back and forth.

* Type the number from 1 to 99 for Scroll Speed, the larger the number
  the faster the speed.

* Start from Left/Right for Horizontal scroll direction only.

* Start from Top/Bottom for Vertical scroll direction only.

* Type any positive number for Loop. The input number stand for the
  number of times will loop. Type "infinite" for infinite loop.

* For CSS cursor style, refer to 
    www.w3schools.com/cssref/pr_class_cursor.asp

-- CUSTOMIZATION --

* People who enable this module SHOULD change the default css selector
  that came with this module. Refer to
  (/sites/all/modules/scroller_block/css/).
  Either modify the original css file directly or override them by the 
  theme's css file.

--TROUBLESHOOTING --

* To submit bug reports and feature suggestions, or to track changes:

    https://drupal.org/project/issues/scroller_block

-- CONTACT --

Current maintainers:
* Jeremy DH Lao (DHL) - https://drupal.org/user/2499500
